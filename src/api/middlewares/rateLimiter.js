const RateLimit = require('express-rate-limit');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const { env, rateLimitTime, rateLimitRequest } = require('../../config/vars');

const handler = (err, req, res, next) => {
  const response = {
    code: err.status,
    message: err.message || httpStatus[err.status],
    errors: err.errors,
    stack: err.stack,
  };
  if (env !== 'development') {
    delete response.stack;
  }
  res.status(err.status);
  res.json(response);
  res.end();
};

const rateLimitHandler = (req, res, next) => {
  const err = new APIError({
    message: 'Se ha superado el límite de peticiones, por favor, inténtelo de nuevo más tarde.',
    status: httpStatus.TOO_MANY_REQUESTS,
  });
  return handler(err, req, res);
}

module.exports = () => {
  if (env === 'production') {
    return new RateLimit({
      windowMs: rateLimitTime * 60 * 1000, // 15 minutos
      max: rateLimitRequest, //  Limitado a 30 peticiones por IP por ventana
      delayMs: 0,
      handler: rateLimitHandler,
    });
  }
  return new RateLimit({
    windowMs: 5 * 60 * 1000, // 5 minutos
    max: 3000, // Limitado a 3000 peticiones por IP por ventana
    delayMs: 0,
    handler: rateLimitHandler,
  });
};
