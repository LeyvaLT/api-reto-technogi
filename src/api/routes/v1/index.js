const express = require('express');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('../../../../swagger_output.json')
const articulos = require('../../services/articulos/articulos.route');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * GET v1/docs
 */
router.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));

router.use('/articulos', articulos);

module.exports = router;
