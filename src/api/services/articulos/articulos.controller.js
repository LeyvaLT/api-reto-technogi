const service = require('./articulos.service');

exports.save = async (req, res, next) => {
  /*
    #swagger.tags = ['Articulos']
    #swagger.path = 'v1/articulos/'
    #swagger.produces = ["application/json"]
    #swagger.consumes = ["application/json"]
    #swagger.description = 'Agregar aticulo'
    #swagger.parameters['data'] = {
      in: 'body',
      description: "Agregar aticulo",
      schema: {
        $title: "Titulo de prueba",
        $text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit...",
      }
    }
  */
  const data = req.body;
  service.save(data)
    .then((response) => res.json({
      ok: true,
      data: {
        _id: response._id,
      },
    }))
    .catch((error) => res.status(500).json({
      ok: false,
      error,
    }));
};

exports.get = async (req, res, next) => {
  /*
    #swagger.tags = ['Articulos']
    #swagger.path = 'v1/articulos/'
    #swagger.description = 'Listar articulos'
  */
  service.get()
    .then((response) => res.json({
      ok: true,
      data: response,
    }))
    .catch((error) => res.status(500).json({
      ok: false,
      error,
    }));
};

exports.delete = async (req, res, next) => {
  /*
    #swagger.tags = ['Articulos']
    #swagger.path = 'v1/articulos/:id'
    #swagger.description = 'Eliminar articulo'
  */
  const { id } = req.params;
  service.delete(id)
    .then(() => res.json({
      ok: true,
    }))
    .catch((error) => res.status(500).json({
      ok: false,
      error,
    }));
};
