const mongoose = require('mongoose');

/**
 * Articulos Schema
 * @private
 */
const articulosSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
}, {
  timestamps: true,
});

/**
 * @typedef Articulos
 */
module.exports = mongoose.model('Articulos', articulosSchema);
