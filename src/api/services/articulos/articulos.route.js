const express = require('express');
const { validate } = require('express-validation');
const controller = require('./articulos.controller');
const { mongoId, save } = require('./articulos.validations');

const router = express.Router();

router.route('/')
  .get(controller.get)
  .post(validate(save), controller.save);

router.route('/:id')
  .delete(validate(mongoId), controller.delete)

module.exports = router;
