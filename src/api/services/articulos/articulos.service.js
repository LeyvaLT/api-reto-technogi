const Articulos = require('./articulos.model');

exports.save = async (data) => {
  try {
    const articulo = new Articulos(data);
    const savedUser = await articulo.save();
    return savedUser;
  } catch (error) {
    throw error(error);
  }
};

exports.get = async () => Articulos.find()
  .then((articulos) => articulos)
  .catch((error) => {
    throw error(error);
  })

exports.delete = async (id) => {
  try {
    return Articulos.remove({ _id: id });
  } catch (error) {
    throw error(error);
  }
};

/*
exports.create = async (data) => {
  try {
  } catch (error) {
    return error;
  }
};

exports.create = async (data) => {
  try {
    const articulo = new Articulos(data);
    const savedUser = await articulo.save();
    return savedUser;
  } catch (error) {
    return error;
  }
};
*/
