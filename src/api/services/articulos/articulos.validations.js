const Joi = require('joi');

module.exports = {
  // Mongo id validate
  mongoId: {
    params: Joi.object({
      id: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
    }),
  },
  save: {
    body: Joi.object({
      title: Joi.string().required(),
      text: Joi.string().required(),
    }),
  },
}
