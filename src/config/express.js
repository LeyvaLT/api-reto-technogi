const express = require('express');
const morgan = require('morgan');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
const helmet = require('helmet');
const routes = require('../api/routes/v1');
const { logs } = require('./vars');
const rateLimiter = require('../api/middlewares/rateLimiter');

/**
* Express instance
* @public
*/
const app = express();

app.use(morgan(logs));

// parse body params
app.use(express.json());
app.use(express.urlencoded({
  extended: true,
}));

// gzip compression
app.use(compress());

app.use(methodOverride());

app.use(helmet());

// enable CORS - Cross Origin Resource
app.use(cors());

// enable rate limit
app.use(rateLimiter());

// mount api v1 routes
app.use('/v1', routes);

module.exports = app;
