Promise = require('bluebird'); // eslint-disable-line no-global-assign
const { port, env } = require('./config/vars');
const app = require('./config/express');
const mongoose = require('./config/mongoose');

// coneccion con mongoose
mongoose.connect();

app.listen(port, () => console.info(`servidor escuchando por el puerto: ${port} (${env})`));

/**
* Exports express
* @public
*/
module.exports = app;
